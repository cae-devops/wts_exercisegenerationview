#pragma once

#include <CameraAspectAPI.h>
#include <InputAPI.h>
#include <TransformationAspectAPI.h>
#include <WorldAPI.h>

#include "Gears.h"
#include "Sample.h"

#include <cmath>

// Limit elevation change if needed
static float64_t maxPitch = 85.0;

class CameraBase
{
protected:
  CameraAspectAPI_v2* _cameraAspectApi;
  InputAPI_v1* _inputApi;
  TransformationAspectAPI_v4* _transformationAspectApi;
  WorldAPI_v5* _worldApi;

  ObjectHandle_v3 _cameraObject;
  bool _sensorToggle;

public:
  CameraBase(CameraAspectAPI_v2* cameraAspectApi, InputAPI_v1* inputApi, TransformationAspectAPI_v4* transformationApi, WorldAPI_v5* worldApi, const GeoPosition_v5& position) :
    _cameraAspectApi(cameraAspectApi),
    _inputApi(inputApi),
    _transformationAspectApi(transformationApi),
    _worldApi(worldApi),
    _cameraObject(kNullObjectHandle),
    _sensorToggle(false)
  {
    if (_worldApi && _transformationAspectApi)
    {
      _worldApi->CreateCamera(&_cameraObject);
      _transformationAspectApi->SetPosition(_cameraObject, position);
    }
  }

  ~CameraBase()
  {
    if (_worldApi && IsValid())
    {
      // No check we don't care if camera was already deleted
      _worldApi->DeleteObject(_cameraObject);
    }
  }

  ObjectHandle_v3 GetHandle() const { return _cameraObject; }
  bool IsValid() const { return _cameraObject.id != kNullObjectHandle.id || _cameraObject.type != kNullObjectHandle.type; }

  float GetInput(const char* inputName)
  {
    float val = 0;
    if (_inputApi) _inputApi->GetInput(inputName, &val);
    return val;
  }

  float GetPlusMinusInput(const char* plusInputName, const char* minusInputName)
  {
    return GetInput(plusInputName) - GetInput(minusInputName);
  }

  void UseCamera()
  {
    if (_worldApi) _worldApi->SetMainCamera(_cameraObject);
  }

  void SetTargetTexture(const char* textureName)
  {
    if (_cameraAspectApi && _cameraObject != kNullObjectHandle) _cameraAspectApi->SetTargetTexture(_cameraObject, textureName);
  }

  static double VectorSize(const Vector3f64_v3& dir)
  {
    return std::sqrt(dir.x * dir.x + dir.y * dir.y + dir.z * dir.z);
  }

  static RotationalAngles_v3 DirectionToRotation(const Vector3f64_v3& dir)
  {
    const auto xz = std::sqrt(dir.x * dir.x + dir.z * dir.z);
    const RotationalAngles_v3 res = { -Rad2Deg * std::atan2(dir.x, dir.z), // yaw
                                      Rad2Deg * std::atan2(dir.y, xz),     // pitch
                                      0.0 };                               // roll

    return res;
  }

  static Vector3f64_v3 RotationToDirection(const RotationalAngles_v3& rotation, double size = 1.0)
  {
    const auto yaw = Deg2Rad * rotation.yaw;
    const auto pitch = Deg2Rad * rotation.pitch;
    const Vector3f64_v3 res = { -size * std::sin(yaw) * std::cos(pitch),  // x
                                size * std::sin(pitch),                   // y
                                size * std::cos(yaw) * std::cos(pitch) }; // z

    return res;
  }
};


class CameraFlying : public CameraBase
{

private:

	float defaultHorizontalFOV = 0.0;
	float defaultVerticalFOV = 0.0;

public:

  CameraFlying(CameraAspectAPI_v2* cameraAspectApi, InputAPI_v1* inputApi, TransformationAspectAPI_v4* transformationApi, WorldAPI_v5* worldApi, const GeoPosition_v5& position) :
    CameraBase(cameraAspectApi, inputApi, transformationApi, worldApi, position) {}

  Vector3f64_v3 TransformXZ(Vector3f64_v3 modelPos)
  {
    const auto orient = GetOrientation(_cameraObject);
    const auto yaw = orient.yaw * Deg2Rad; // Convert to radians
    const Vector3f64_v3 forward = { std::sin(yaw),   // x
                                    0.0,             // y
                                    std::cos(yaw) }; // z
    const Vector3f64_v3 right = { std::cos(yaw),    // x
                                  0.0,              // y
                                  -std::sin(yaw) }; // z
    const Vector3f64_v3 res = { modelPos.z * forward.x + modelPos.x * right.x,   // x
                                modelPos.y,                                      // y
                                modelPos.z * forward.z + modelPos.x * right.z }; // z

    return res;
  }


  void InitialiseFOV(const float& horizontalFOV, const float& verticalFOV)
  {
	  //It is not clear how the single angle parameter passed to SetFOV() relates to the horizontal/vertical FOV settings
	  //Just use the horizontal value for now
	  defaultHorizontalFOV = horizontalFOV * (float)Deg2Rad;
	  defaultVerticalFOV = verticalFOV * (float)Deg2Rad;;
	  SDKCheck(Gears::API::CameraAspectAPIv2()->SetFOV(_cameraObject, std::atan(defaultHorizontalFOV / 2.0f)));
  }


  void DDSUpdateFov(const double& zoomlevel)
  {
	  float zoomFactor = (1.0F / (float)zoomlevel);
	  float newAngle = (defaultHorizontalFOV / 2.0f) * zoomFactor;
	  SDKCheck(Gears::API::CameraAspectAPIv2()->SetFOV(_cameraObject, std::tan(newAngle)));
  }


  void DDSRepositionCamera(GeoPosition_v5 position)
  {
	 _transformationAspectApi->SetPosition(_cameraObject, position);
  }


  GeoPosition_v5  DDSGetCameraPosition(void) const
  {
	  const auto pos = GetPosition(_cameraObject);
	  GeoPosition_v5 res = { pos.latitude, pos.longitude, pos.altitude };

	  return res;
  }


  RotationalAngles_v3  DDSGetCameraOrientation(void) const
  {
	  const auto orient = GetOrientation(_cameraObject);
	  return orient;
  }


  void DDSReorientateCamera(const double& heading, const double& pitch)
  {
	  const auto curretOrientation = GetOrientation(_cameraObject);
	  const RotationalAngles_v3 newOrientation = { heading, Clamp(pitch, -maxPitch, maxPitch), curretOrientation.roll };
	  _transformationAspectApi->SetOrientation(_cameraObject, newOrientation);
  }

};

class CameraOrbital : public CameraBase
{
  ObjectHandle_v3 _target;

public:
  CameraOrbital(CameraAspectAPI_v2* cameraAspectApi, InputAPI_v1* inputApi, TransformationAspectAPI_v4* transformationApi, WorldAPI_v5* worldApi, const GeoPosition_v5& position) :
    CameraBase(cameraAspectApi, inputApi, transformationApi, worldApi, position), _target(kNullObjectHandle) {}

  Vector3f32_v3 ViewOffset() const { Vector3f32_v3 res = {0, 1.5f, 0}; return res; }

  void CameraOrbital::SetTarget(ObjectHandle_v3& target)
  {
    if (_target == target) return;
    if (!IsValid() || !_transformationAspectApi || !_cameraAspectApi) return;

    _target = target;
    Vector3f32_v3 off = ViewOffset();
    _transformationAspectApi->SetAttachedTo(_cameraObject, target);
    _cameraAspectApi->LockToObject(_cameraObject, target, &off);
  }

  Vector3f64_v3 ComputeNewModelPosition(float yawChange, float pitchChange, float distChange) const
  {
    // Prepare variables needed for the computation
    const auto wPos = GetPosition(_cameraObject);
    Vector3f64_v3 model_position;
    const auto off = ViewOffset();

    _transformationAspectApi->WorldToModel(_target, wPos, &model_position);

    // remove view offset
    const Vector3f64_v3 mPos = { model_position.x - off.x,   // x
                                 model_position.y - off.y,   // y
                                 model_position.z - off.z }; // z
    const auto rot = DirectionToRotation(mPos);

    // Do not go below ground nor try to move over the top
    const RotationalAngles_v3 rotation = { rot.yaw + yawChange,                                                                                             // yaw
                                           pitchChange < 0.0F && mPos.y < -2.0 || pitchChange + rot.pitch > maxPitch ? rot.pitch : rot.pitch + pitchChange, // pitch
                                           rot.roll };                                                                                                      // roll
    const auto distance = Clamp(VectorSize(mPos) * (1.0F + distChange), 1.0, 100.0);

    // Apply the changes
    const auto output = RotationToDirection(rotation, distance);
    const Vector3f64_v3 result = { output.x + off.x,   // x
                                   output.y + off.y,   // y
                                   output.z + off.z }; // z

    return result; // return view offset
  }

};