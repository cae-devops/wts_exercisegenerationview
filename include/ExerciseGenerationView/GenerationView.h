#pragma once

#include "EntityControl.h"
#include "Camera.h"
#include "ExerciseGenerationView.h"
#include "StealthUpdate.h"

#include <CommonTypesAPI.h>

#ifdef _DEBUG
#define SDKCheck(function) assert(APIRESULT_SUCCESS(function) == TRUE)
#else
#define SDKCheck(function) function
#endif

class GenerationView : public Sample
{
public:
  enum class Actions {None, MovingXY, MovingXZ, Rotating, Attaching};

  GenerationView(OSWindowAPI_v3* main_window)
    : Sample("Generation")
    , _mission_started(false)
    , _in_mission(false)
    , _buttons(0)
    , _camera(nullptr)
	, _selected(kNullObjectHandle)
    , _selectedMarker(-1)
    , _lastXY()
    , _client_width(0)
    , _client_height(0)
    , _main_window(main_window)
  {}

  virtual bool Init() override;
  virtual void OnBeforeSimulation(float delta) override;
  virtual void OnMissionStart() override;
  virtual void Cleanup() override;

  virtual void OnMissionEnd() override { Cleanup(); }
  bool OnSize(const int32_t client_width, const int32_t client_height) override;
  virtual bool OnMouseButtonPressed(ButtonID_v1 button_id, int32_t client_x, int32_t client_y) override;
  virtual bool OnMouseButtonReleased(ButtonID_v1 button_id, int32_t client_x, int32_t client_y) override;
  virtual bool OnMouseDoubleClick(ButtonID_v1 button_id, int32_t client_x, int32_t client_y) override;
  virtual bool OnKeyPressed(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags) override;
  virtual void OnRenderMainWindow() override;

  void SetSelected(ObjectHandle_v3 object);
  void UpdateMarker();
  void AttachTo(ObjectHandle_v3 parent);
  void Move(const Vector3f64_v3& param);
  void Rotate(const RotationalAngles_v3& diff);

  //DDS message stuff
  void InitialiseDDS();
  bool CreateObject(float64_t ipLat, float64_t ipLon, long csaObjID, long objectType, double heading);
  void DeleteObject(long csaObjId);
  void DeleteAllObjects();
  void ControlObjectPosition(long csaObjId,int entityGoToID);
  bool UpdateObject (long csaObjId, float64_t ipLat, float64_t ipLon,double heading);
  void ConvertENUToLLA(const double& X, const double& Y, const double& Z);
  void ConvertLLACameraPositionToENU(void);
  void AddNewObject(ObjectHandle_v3 obj, long csaObjectId);

  void SQFDisableActionMenu();
  void SQFEnabledActionMenu();
  void SQFDisableUserInput();

  std::pair<bool, GeoPosition_v5> GenerationView::NewPosition(const float64_t& ipLat, const float64_t& ipLon);

  void UpdateExgenObjects(float delta);
  void SelectDeselectObjects(long objID, bool select);
  void UpdateCameraView(float delta);
  void sendUpdatesToCIOS();


private:

  void ProcessStealthControlInputs(void); 
  void CalculateCameraOrientation(const float& timeDelta);
  void CalculateCameraMovement(const float& timeDelta);
  void MoveCameraForward(const double& distance);
  void UpdateCamera(const float& timeDelta);
  bool RepositionCamera(StealthUpdate::S_ECEF_POSITION position);
  void AlignCamera(StealthUpdate::S_ECEF_POSITION position);
  bool ObjectAlreadyExists(long csaObjectId);
 
  bool32_t enable = true;
  bool32_t disable = false;

  ObjectHandle_v3 _new_obj;
  bool DDSinitialised = false;
  bool cameraCreated = false;
  Vbs3ApplicationState_v1 state;
  bool _mission_started, _in_mission;
  int _buttons;
  double cameraHeightoffset = 0.0;
  CameraFlying* _camera;
  ObjectHandle_v3 _selected;
  int64_t _selectedMarker;
  ScreenPosition_v3 _lastXY;
  int32_t _client_width;
  int32_t _client_height;
  OSWindowAPI_v3* _main_window;

  // Entity stuff
  std::vector<ObjectHandle_v3> _tank_group;
  void CreateTankCrewAndTurret(ObjectHandle_v3 vehicle, GeoPosition_v5 position, long csaObjID);
  void CreateTankGroup(int32_t force_type);
  ObjectHandle_v3 _vehicle, _gunner, _driver, _commander, _vehicle_turret;
  std::vector<ObjectHandle_v3> _vehicle_driver;
  std::vector<ObjectHandle_v3> _vehicle_with_driver;
  ObjectHandle_v3 _dead_player = { 0,0 };
  ObjectHandle_v3 _ownship = { 0,0 }; 
  int _no_of_tanks = 0;

  std::vector<ObjectHandle_v3> _vbs_obj_identifier;
  std::vector<long> _csa_obj_identifier;

  // ECEF Lat Lon ENU stuff

  double ECEF_x = 0.0;
  double ECEF_y = 0.0;
  double ECEF_z = 0.0;
  double* ptr_ECEF_x = &ECEF_x;
  double* ptr_ECEF_y = &ECEF_y;
  double* ptr_ECEF_z = &ECEF_z;
  double ENU_x = 0.0;
  double ENU_y = 0.0;
  double ENU_z = 0.0;
  double* ptr_ENU_x = &ENU_x;
  double* ptr_ENU_y = &ENU_y;
  double* ptr_ENU_z = &ENU_z;
  GeoPosition_v5 latLongPosition = { 0.0 , 0.0, 0.0 };
  float64_t radsToMilliRads = 1000.0;
  bool cameraUpdated = false;
  bool newCameraOrientation = false;
  double currentHeadingRate = 0.0;
  double currentPitchRate = 0.0;
  double currentCameraPitch = 0.0;
};
