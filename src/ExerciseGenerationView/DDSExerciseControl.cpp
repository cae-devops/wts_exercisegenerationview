#include "DDSExerciseControl.h"
#include "GenerationMessageData.h"

void DDSExerciseControl::StartExerciseGeneration(void)
{
	//std::cout << " ************************************** DDSExerciseControl::StartExerciseGeneration **************************************" << std::endl;
	exgenStart = true;
	exconStartTrainingSession = false;
}

void DDSExerciseControl::LeaveExerciseGeneration(void)
{
	//std::cout << " ************************************** DDSExerciseControl::LeaveExerciseGeneration **************************************" << std::endl;
	exgenLeave = true;
}

void DDSExerciseControl::LoadExercise(const std::string& exerciseName, const std::string& exerciseFile)
{
	//std::cout << " *** DDSExerciseControl::LoadExercise ***  " << std::endl;
	//std::cout << "     Exercise Name : " << exerciseName << std::endl;
	//std::cout << "     Exercise File : " << exerciseFile.substr(0, 10) << std::endl;
	
}

void DDSExerciseControl::StartExercise(void)
{
	//exconStartExercise = true;
}

void DDSExerciseControl::PauseExercise(void)
{
	//exconPauseExercise = true;
}

void DDSExerciseControl::ResumeExercise(void)
{
	//exconResumeExercise = true;
}

void DDSExerciseControl::StopExercise(void)
{
	//exconStopExercise = true;
}

void DDSExerciseControl::LoadVoiceCommand(const std::string& voiceCommandName, const std::string& voiceCommandFile)
{
	//std::cout << " *** DDSExerciseControl::LoadVoiceCommand ***" << std::endl;
	//std::cout << "     Voice Command Name : " << voiceCommandName << std::endl;
	//std::cout << "     Voice Command File : " << voiceCommandFile.substr(0, 10) << std::endl;
}

void DDSExerciseControl::StartTrainingSession(void)
{
	exconStartTrainingSession = true;
	exgenStart = false;
}

void DDSExerciseControl::LeaveTrainingSession(void)
{
	//exconLeaveTrainingSession = true;
	exconStartTrainingSession = false;
}

void DDSExerciseControl::CourseComplete(void)
{
	//exconCourseComplete = true;
}

void DDSExerciseControl::PhaseComplete(void)
{
	//exconPhaseComplete = true;
}

void DDSExerciseControl::DisplayMessage(const std::string& message)
{
	//exconDisplayMessage = message;
}

void DDSExerciseControl::CGFOwnership(const unsigned long& ovEntityId, const unsigned long& numOfEntityIds, const unsigned long *entityId)
{
	//std::cout << " *** DDSExerciseControl::CGFOwnership ***" << std::endl;
	//std::cout << "     OV Entity ID: " << ovEntityId << std::endl;
	//
	//for (unsigned long i = 0; i < numOfEntityIds; i++)
	//{
	//	std::cout << "     Entity ID  : " << i << " = " << entityId[i] << std::endl;
	//}
	//}
}