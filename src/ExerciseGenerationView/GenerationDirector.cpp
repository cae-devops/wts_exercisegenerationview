/*
*
* Copyright(c) 2018 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/

#include "GenerationDirector.h"
#include <string>
#include "GenerationView.h"
#include "Gears.h"
#include "GenerationMessageData.h"

#define LISTENER_PRIORITY 5000

APIResult GenerationDirector::Initialize(APIManager_v6* api_manager, NativeModuleHandle proxy_handle)
{
	bool allFound = true;

	// Start service listener
	if (APIRESULT_FAIL(Gears::API::OSServiceAPIv6()->GetMainWindow(&_main_window, kTimeoutInfinite))
		|| APIRESULT_FAIL(_main_window->AddListener(_main_window, Gears::MyAPI::OSWindowListenerAPIv3(), LISTENER_PRIORITY)))
	{
		allFound = false;
	}

	// Allocate APIs
	auto log_api = Gears::API::LogAPIv1();

	log_api->LogInfo(log_api, "EXGEN loading.");

	if (allFound)
	{
		log_api->LogInfo(log_api, "OK.");
	}
	else
	{
		log_api->LogInfo(log_api, "Error occurred.");
	}

	_api_manager = api_manager;
	_proxy_handle = proxy_handle;

	_generation_view = new GenerationView(_main_window);

	return allFound ? kAPIResult_GeneralSuccess : kAPIResult_GeneralError;
}

void GenerationDirector::OnBeforeSimulation(float delta)
{
	if (_generation_view != nullptr)
	{
		_generation_view->OnBeforeSimulation(delta);

		Vbs3ApplicationState_v1 state;
		SDKCheck(Gears::API::VBS3ApplicationAPIv2()->GetCurrentState(&state));

		if (!_generationview_started && state == kVbs3ApplicationState_main_menu)
		{
			// Load empty mission
			_generation_view->Init();
			_generationview_started = true;
		}
	}
}

void GenerationDirector::OnAfterSimulation(float delta)
{
	if (_generation_view != nullptr)
	{
		_generation_view->OnAfterSimulation(delta);
	}
}

void GenerationDirector::OnMissionStart()
{
	if (_generation_view != nullptr)
	{
		// Show mouse cursor if some gui is drawn
		SDKCheck(Gears::API::VBS3GUIAPIv2()->SetCursorType(kVBS3CursorType_arrow));

		_generation_view->OnMissionStart();
	}
}

void GenerationDirector::OnMissionEnd()
{
	if (_generation_view != nullptr)
	{
		_generation_view->OnMissionEnd();
	}
}

void GenerationDirector::OnMissionLoad(const char* mission_name)
{
	if (_generation_view != nullptr)
	{
		_generation_view->OnMissionLoad(mission_name);
	}
}

void GenerationDirector::OnRenderMainWindow()
{
	Vbs3ApplicationState_v1 state;

	SDKCheck(Gears::API::VBS3ApplicationAPIv2()->GetCurrentState(&state));

	if (state == kVbs3ApplicationState_unknown || state == kVbs3ApplicationState_shutdown)
	{
		return;
	}

	SetStyles();

	if (_generation_view != nullptr)
	{
		_generation_view->OnRenderMainWindow();
	}

	// Set size for buttons we will use
	ImVec2_v1 size;
	size._x = 180;
	size._y = 25;

	// Show mouse cursor if some gui is drawn
	SDKCheck(Gears::API::VBS3GUIAPIv2()->SetCursorType(kVBS3CursorType_arrow));

	bool32_t pressed = FALSE;

	if (!_generationview_started)
	{
		SDKCheck(Gears::API::IMGuiAPIv1()->Button("Warrior View", size, &pressed));
		if (pressed == TRUE || exgenStart)
		{
			_generation_view->Init();
			_generationview_started = true;
		}
	}
	else
	{
		SDKCheck(Gears::API::IMGuiAPIv1()->Button("End Warrior View", size, &pressed));
		if (pressed == TRUE || exgenLeave)
		{
			_generation_view->Cleanup();
			_generationview_started = false;
		}
	}
}

// Sets DebugUI style
void GenerationDirector::SetStyles()
{
	ImGuiStyle_v1* pStyle;

	SDKCheck(Gears::API::IMGuiAPIv1()->GetStyle(&pStyle));

	pStyle->_WindowRounding = 0;
	pStyle->_ChildWindowRounding = 0;
	pStyle->_ScrollbarRounding = 0;
	pStyle->_Alpha = 0.9f;
	pStyle->_AntiAliasedLines = TRUE;
	pStyle->_AntiAliasedShapes = TRUE;
	pStyle->_ScrollbarSize = 14;

	const ImVec4_v1 main_color = { 0.937F, // _x
								   0.521F, // _y
								   0.129F, // _z
								   1.0F }; // _w

	pStyle->_Colors[kImGuiCol_CheckMark] = main_color;
	pStyle->_Colors[kImGuiCol_FrameBgHovered] = main_color;
	pStyle->_Colors[kImGuiCol_ButtonHovered] = main_color;

	const ImVec4_v1 scrollbar_color = { 0.15F,  // _x
										0.15F,  // _y
										0.15F,  // _z
										1.0F }; // _w

	pStyle->_Colors[kImGuiCol_ScrollbarGrabActive] = scrollbar_color;
	pStyle->_Colors[kImGuiCol_ScrollbarGrabHovered] = scrollbar_color;

	const ImVec4_v1 button_color = { 0.5F,   // _x
									 0.5F,   // _z
									 0.5F,   // _y
									 1.0F }; // _w

	pStyle->_Colors[kImGuiCol_Button] = button_color;
}

bool32_t GenerationDirector::OnSize(const int32_t client_width, const int32_t client_height)
{
	return _generation_view != nullptr && _generation_view->OnSize(client_width, client_height) ? TRUE : FALSE;
}

bool32_t GenerationDirector::OnMouseButtonPressed(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
	//return _generation_view != nullptr && _generation_view->OnMouseButtonPressed(button_id, client_x, client_y) ? TRUE : FALSE;
	return FALSE;
}

bool32_t GenerationDirector::OnMouseButtonReleased(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
	//return _generation_view != nullptr && _generation_view->OnMouseButtonReleased(button_id, client_x, client_y) ? TRUE : FALSE;
	return FALSE;
}

bool32_t GenerationDirector::OnMouseWheel(WheelID_v1 wheel_id, int32_t wheel_delta)
{
	//return _generation_view != nullptr && _generation_view->OnMouseWheel(wheel_id, wheel_delta) ? TRUE : FALSE;
	return FALSE;
}

bool32_t GenerationDirector::OnKeyPressed(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags)
{
	return _generation_view != nullptr && _generation_view->OnKeyPressed(key_id, system_key, scan_code, scan_flags) ? TRUE : FALSE;
}

bool32_t GenerationDirector::OnKeyReleased(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags)
{
	//return _generation_view != nullptr && _generation_view->OnKeyReleased(key_id, system_key, scan_code, scan_flags) ? TRUE : FALSE;
	return FALSE;
}

bool32_t GenerationDirector::OnMouseMove(int32_t client_x, int32_t client_y)
{
	if (_generation_view == nullptr)
	{
		return FALSE;
	}
	else
	{
		const ScreenPosition_v3 client_xy = { static_cast<float32_t>(client_x),   // x
											  static_cast<float32_t>(client_y) }; // y

		//return _generation_view->OnMouseMove(client_xy) ? TRUE : FALSE;
	}
	return FALSE;
}

bool32_t GenerationDirector::OnMouseDoubleClick(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
	//return _generation_view != nullptr && _generation_view->OnMouseDoubleClick(button_id, client_x, client_y) ? TRUE : FALSE;
	return FALSE;
}

void GenerationDirector::OnObjectCollision(ObjectHandle_v3 object, ObjectHandle_v3 trigger_object, CollisionEventType_v1 type, CollisionParameters_v1 params, CollisionParameters_v1 trigger_params)
{
	if (_generation_view) _generation_view->OnObjectCollision(object, trigger_object, type, params, trigger_params);
}

void GenerationDirector::OnObjectCreation(ObjectHandle_v3 object)
{
	if (_generation_view) _generation_view->OnObjectCreation(object);
}

void GenerationDirector::OnObjectDeletion(ObjectHandle_v3 object)
{
	if (_generation_view) _generation_view->OnObjectDeletion(object);
}
