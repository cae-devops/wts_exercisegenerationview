#include "GenerationView.h"

#include <IMGuiAPI.h>
#include <ModelAspectAPI.h>
#include <OSServiceListenerAPI.h>
#include <SimulationAspectAPI.h>
#include <TransformationAspectAPI.h>
#include <VBS3ApplicationAPI.h>
#include <VBS3GUIAPI.h>
#include <WorldAPI.h>

#include "StealthUpdateTransmitter.h"
#include "StealthControl.h"
#include "DDSTransmitterInterface.h"

#include "EntityControlReceiver.h"
#include "DDSEntityControl.h"
#include "GenerationDirector.h"
#include "GenerationMessageData.h"

#include <cmath>
#include <string>
#include <utility>
#include <stdlib.h>

//
// ****** Entity Control *******
//
bool addEntities = false; 
std::vector<long> entityAddEntityTypeId = { 0 };
std::vector<long> entityAddObjectIDs;
std::vector <EntityControl::S_OBJECT_LOCATION_AND_HEADING> entityAddLocationAndHeading = { 0,{0.0,0.0,0.0} };
long entityRemoveObjectIdentifier = 0;
std::vector<long> entityGoToObjectIdentifier;
std::vector<EntityControl::S_ECEF_POSITION> entityGoToLocation;
std::vector<double> entityGoToSpeed;
std::vector<double> entityGoToDwellTime;
unsigned long exconStopEntityMovementID = 0;
unsigned long exconMakeInvulnerableEventID = 0;
unsigned long exconMakeVulnerableEventID = 0;
unsigned long exconKillEntityID = 0;
//
// ****** Stealth Update *******
//
StealthUpdateTransmitter* stealthUpdateTransmitterCopy; 

//Stealth parameters - need to be configurable 
#define MAX_HEIGHT_OFFSET 200
#define MIN_HEIGHT_OFFSET 2.575 //Height of the Warrior Sight
#define MAX_ZOOM_LEVEL 10.0
#define HORIZONTAL_FOV 30.0
#define VERTICAL_FOV 30.0
#define MAX_SPEED 100.0
#define MAX_ACCELERATION 20.0
#define MAX_HEADING_RATE PI / 2.0
#define MAX_PITCH_RATE PI / 2.0


//SHREWTON map
//Vector3f64_v3 mapOrigin{ 34400.02, 0.0, 19399.98 };  // note Y an Z are transposed

//SALS map
Vector3f64_v3 mapOrigin{ 24400.0, 0.0, 19400.0 };  // note Y an Z are transposed

//
// ****** Exercise Control *******
// ***** Only exgen start and leave messages required here *****
//
bool exgenStart = false; //????????????????
bool exgenLeave = false;  //???????????????????
bool exconStartTrainingSession = false;
						  //
// ****** Exercise Generation *******
//
std::vector <ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER> exgenAddObjectIdentifier;
std::vector<long> exgenObjectSubTypeId;
std::vector <ExerciseGeneration::S_OBJECT_LOCATION_AND_HEADING> exgenAddObjectLocationAndHeading;
ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER exgenUpdateObjectIdentifier = { 0,0 };
ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER exgenSelectObjectIdentifier = { 0,0 };
ExerciseGeneration::S_UNIQUE_OBJECT_IDENTIFIER exgenDeselectObjectIdentifier = { 0,0 };
ExerciseGeneration::S_OBJECT_LOCATION_AND_HEADING exgenUpdateObjectLocationAndHeading;
std::vector<int> exgenRemoveObjectIDs;
std::vector<int> exgenAddObjectIDs;
bool addObjects = false;
bool removeAllObjects = false;
std::string exgenTrackName = "track1";
std::string exgenTrackFile = "track1.xml";
bool exgenPauseTrack = false;
bool exgenResumeTrack = false;
bool exgenStartTrack = false;
bool exgenStopTrack = false;

//
// ****** Stealth Control *******
//
double stealthSpeedInput = 0.0;
std::vector<double> stealthBodyHeadingRate;
std::vector<double> stealthBodyPitchRate;
std::vector<double> stealthHeadingRate;
std::vector<double> stealthPitchRate;
std::vector<double> stealthHeightOffset;
std::vector<double> stealthZoomLevel;
double stealthHeading = 0.0;
unsigned short setViewToEntityID = 0;
bool stealthRepositionCamera = false;
bool stealthReorientateCamera = false;
bool stealthHeightChange = false;
bool stealthSpeedChange = false;
bool stealthZoom = false;
bool stealthRotation = false;
bool stealthAlignCamera = false;
double requiredStealthSpeed = 0.0;
double currentStealthSpeed = 0.0;
double totalDistanceTravelled = 0.0;

StealthUpdate::S_ECEF_POSITION stealthObjectLocation = { 0.0,0.0,0.0 };
StealthUpdate::S_ECEF_POSITION stealthAlignmentLocation = { 0.0,0.0,0.0 };

//
// ****** Receive DDS messages ****** 
//
#include "ExerciseControlReceiver.h"
#include "ExerciseGenerationReceiver.h"
#include "StealthControlReceiver.h"
#include "DDSStealthControl.h"
#include "DDSExerciseControl.h"
#include "DDSExerciseGeneration.h"
#include "DDSReceiverInterface.h"

#include "ScopeLock.h"
SharedPtr<LockObject> MessageLock;

//
//  ****** Conversion values ******
//
double radsToDegs = 57.295779513;
double degsToRads = 0.01745329252;
double KMHToMPS = 0.277777778;
double MPSToKMH = 3.6;

bool enable_log_output = false;

void ShowAndLog(const char* str)
{
	if (enable_log_output)
	{
	  SDKCheck(Gears::API::GUIAPIv1()->DisplayMessage(str));
	  auto log_api = Gears::API::LogAPIv1();
	  log_api->LogInfo(log_api, str);
	}
}

bool GenerationView::Init()
{
  SDKCheck(_main_window->GetClientWidth(_main_window, &_client_width));
  SDKCheck(_main_window->GetClientHeight(_main_window, &_client_height));

  SDKCheck(Gears::API::VBS3ApplicationAPIv2()->GetCurrentState(&state));
  _in_mission = state == kVbs3ApplicationState_mission;

  if (!_in_mission)
  {
	  // Start empty mission
	  return APIRESULT_SUCCESS(Gears::API::MissionAPIv3()->StartEmptyMission("tt_spta_dvs", TRUE));
  }

  return true;
}

void GenerationView::OnMissionStart()
{
	// Set up environment
	float32_t overcast_value = 0.0;
	SDKCheck(Gears::API::EnvironmentAPIv1()->SetOvercast(overcast_value, 0.0));
	SDKCheck(Gears::API::EnvironmentAPIv1()->EnableAmbientLife(disable));

	// Stop some mouse actions
	SQFDisableActionMenu();

	_mission_started = true;
}

void GenerationView::OnRenderMainWindow()
{
	const char text[] =
		"EXGEN baby"
		;
	bool32_t opened = TRUE;

	SDKCheck(Gears::API::IMGuiAPIv1()->Begin("EXGEN information", kImGuiWindowFlags_AlwaysAutoResize, &opened, &opened));
	SDKCheck(Gears::API::IMGuiAPIv1()->Text(text));
	SDKCheck(Gears::API::IMGuiAPIv1()->End());
}

void GenerationView::OnBeforeSimulation(float delta)
{
  // Wait till we really get to mission
  SDKCheck(Gears::API::VBS3ApplicationAPIv2()->GetCurrentState(&state));
  _in_mission = state == kVbs3ApplicationState_mission;

  if (!_mission_started || !_in_mission)
  {
    return;
  }
  
  if (!DDSinitialised)
  {
	  SharedPtr<LockObject> messageLock(new LockObject);
	  MessageLock = messageLock;

	  InitialiseDDS();
	  DDSinitialised = true;
  }

  if (exgenStart && !exconStartTrainingSession)
  {
	  if (!cameraCreated)
	  {
		  // Create new flying camera
		  const GeoPosition_v5 position = { 51.18493846, -1.89617157, 120.0 };

		  _camera = new CameraFlying(Gears::API::CameraAspectAPIv2(), Gears::API::InputAPIv1(), Gears::API::TransformationAspectAPIv4(), Gears::API::WorldAPIv5(), position);
		  _camera->UseCamera();
		  _camera->InitialiseFOV(HORIZONTAL_FOV, VERTICAL_FOV);
		  cameraCreated = true;

		  SQFDisableUserInput();

		  removeAllObjects = true;
		  DeleteAllObjects();
	  }

	  ProcessStealthControlInputs();
	  UpdateCamera(delta);
	  UpdateCameraView(delta);
	  UpdateExgenObjects(delta);
  }
  else
  {
      // Clear camera data ready for exercise control view
	  if (cameraCreated)
	  {
		  delete _camera;
		  _camera = nullptr;
		  cameraCreated = false;
		  SetSelected(kNullObjectHandle);
	  }
  }
}

void GenerationView::InitialiseDDS()
{
	// *** INITIALISE DDS HERE BECAUSE OF DLL LOAD PROBLEMS ***
	// ****** DDS Receiver Interface *******
	DDSReceiverInterface* ddsReceiverInterface = new DDSReceiverInterface();

	// ****** Exercise Control *******
	// ***** Only exgen start and leave messages required here *****
	DDSExerciseControl* ddsExerciseControl = new DDSExerciseControl;
	ExerciseControlReceiver* exerciseControlReceiver = ddsReceiverInterface->GetExerciseControlReceiver();
	exerciseControlReceiver->Initialise(ddsExerciseControl);

	// ****** Exercise Generation *******
	DDSExerciseGeneration* ddsExerciseGeneration = new DDSExerciseGeneration;
	ExerciseGenerationReceiver* exerciseGenerationReceiver = ddsReceiverInterface->GetExerciseGenerationReceiver();
	exerciseGenerationReceiver->Initialise(ddsExerciseGeneration);
	
	// ****** Entity Control *******
	DDSEntityControl*   ddsEntityControl = new DDSEntityControl;
	EntityControlReceiver* entityControlReceiver = ddsReceiverInterface->GetEntityControlReceiver();
	entityControlReceiver->Initialise(ddsEntityControl);

	// ****** Stealth Control *******
	DDSStealthControl* ddsStealthControl = new DDSStealthControl;
	StealthControlReceiver*  stealthControlReceiver = ddsReceiverInterface->GetStealthControlReceiver();
	stealthControlReceiver->Initialise(ddsStealthControl);

	// ****** DDS Transmitter Interface *******
	DDSTransmitterInterface* ddsTransmitterInterface = new DDSTransmitterInterface;

	// ****** Stealth Update *******
	StealthUpdateTransmitter* stealthUpdateTransmitter = ddsTransmitterInterface->GetStealthUpdateTransmitter();
	stealthUpdateTransmitter->Initialise();

	stealthUpdateTransmitterCopy = stealthUpdateTransmitter;
}

void GenerationView::UpdateExgenObjects(float delta)
{
	ScopeLock lock(MessageLock);
	if (removeAllObjects)
	{
		DeleteAllObjects();
	}
	else if (exgenRemoveObjectIDs.size() > 0)
	{
		for (int i = 0; i < exgenRemoveObjectIDs.size(); i++)
		{
			DeleteObject(exgenRemoveObjectIDs[i]);
		}
		exgenRemoveObjectIDs.clear();
	}

	if (addObjects)
	{
		for (int i = 0; i < exgenAddObjectIDs.size(); i++)
		{
			if (ObjectAlreadyExists(exgenAddObjectIDs[i]))
			{
				exgenAddObjectIDs[i] = 0;
				ShowAndLog("ADD visual object already exists");
			}
			else if (exgenAddObjectIDs[i] > 0)
			{
				ConvertENUToLLA(exgenAddObjectLocationAndHeading[i].position.ECEF_X, exgenAddObjectLocationAndHeading[i].position.ECEF_Y, exgenAddObjectLocationAndHeading[i].position.ECEF_Z);

				if (CreateObject(latLongPosition.latitude, latLongPosition.longitude, exgenAddObjectIDs[i], exgenObjectSubTypeId[i], exgenAddObjectLocationAndHeading[i].heading))
					exgenAddObjectIDs[i] = 0;
			}
		}
		addObjects = false;
		exgenAddObjectIDs.clear();
		exgenAddObjectIdentifier.clear();
		exgenObjectSubTypeId.clear();
		exgenAddObjectLocationAndHeading.clear();
	}
	else if (addEntities)
	{
		for (int i = 0; i < entityAddObjectIDs.size(); i++)
		{
			if (entityAddObjectIDs[i] > 0)
			{
				ConvertENUToLLA(entityAddLocationAndHeading[i].position.ECEF_X,
					entityAddLocationAndHeading[i].position.ECEF_Y,
					entityAddLocationAndHeading[i].position.ECEF_Z);

				if(CreateObject(latLongPosition.latitude, latLongPosition.longitude, entityAddObjectIDs[i], entityAddEntityTypeId[i],entityAddLocationAndHeading[i].heading))
					entityAddObjectIDs[i] = 0;
			}
		}

		addEntities = false;
		entityAddObjectIDs.clear();
		entityAddEntityTypeId.clear();
		entityAddLocationAndHeading.clear();
	}
	else if (entityGoToObjectIdentifier.size() > 0 && _csa_obj_identifier.size() > 0)
	{

		for (int i = 0; i < entityGoToObjectIdentifier.size(); i++)
		{
			if (ObjectAlreadyExists(entityGoToObjectIdentifier[i]))
			{
				ControlObjectPosition(entityGoToObjectIdentifier[i], i);
				entityGoToObjectIdentifier.erase(entityGoToObjectIdentifier.begin() + i);
				entityGoToLocation.erase(entityGoToLocation.begin() + i);
				entityGoToSpeed.erase(entityGoToSpeed.begin() + i);
				entityGoToDwellTime.erase(entityGoToDwellTime.begin() + i);
			}
		}
	}
	else if (exgenUpdateObjectIdentifier.objectId != 0)
	{
		ConvertENUToLLA(exgenUpdateObjectLocationAndHeading.position.ECEF_X, exgenUpdateObjectLocationAndHeading.position.ECEF_Y, exgenUpdateObjectLocationAndHeading.position.ECEF_Z);

		if (UpdateObject(exgenUpdateObjectIdentifier.objectId, latLongPosition.latitude, latLongPosition.longitude, exgenUpdateObjectLocationAndHeading.heading))
		{
			exgenUpdateObjectIdentifier = { 0, 0 };
			exgenUpdateObjectLocationAndHeading = { 0,0,0 };
		}
	}

	// Select/Deselect objects in visual scene
	if (exgenDeselectObjectIdentifier.objectId)
	{
		SelectDeselectObjects(exgenDeselectObjectIdentifier.objectId, false);
		exgenDeselectObjectIdentifier = { 0, 0 };
	}

	else if (exgenSelectObjectIdentifier.objectId)
	{
		SelectDeselectObjects(exgenSelectObjectIdentifier.objectId, true);
		exgenSelectObjectIdentifier = { 0, 0 };
	}
}

bool GenerationView::CreateObject(float64_t ipLat, float64_t ipLon, long csaObjID, long objectType, double heading)
{
	RotationalAngles_v3 orientation;
	bool tank_object = false;

	std::pair<bool, GeoPosition_v5> entity_position = NewPosition(ipLat, ipLon);

	if (entity_position.first)
	{
		if (objectType == (long)1.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Army_T80U_W_X", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)6.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_ARMY_BRDM_W_X", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)13.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Army_bmp1_D_X", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)19.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs_dvs_op_army_hip_k_wdl_x", entity_position.second, &_new_obj));
		}
		else if (objectType == (long)45.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_ZZ_Army_T62A_W", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)52.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs_opfor_2s19_grn_x", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)56.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Army_ZSU_23_4_W_X", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)57.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Ural_4320_W_Ammo_X", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)59.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs_se_army_leader_wdl_M_medium_fmv1_mopp0_ak5c_helmet", entity_position.second, &_new_obj));
		}
		else if (objectType == (long)62.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("cae_fv510_wdl_x", entity_position.second, &_new_obj));
			if (!ValidateHandle(_ownship)) _ownship = _new_obj;
			tank_object = true;
		}
		else if (objectType == (long)65.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_CZ_Army_T72_D", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)67.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_OPFOR_Army_BMP3_W_X", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)70.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_AU_Army_ARH_AntiTank_W_X", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)74.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_GB_RAF_CH47_G_M134_X", entity_position.second, &_new_obj));
			tank_object = true;
		}
		else if (objectType == (long)104.0 || objectType == (long)230.0 || objectType == (long)231.0 || objectType == (long)108.0 || objectType == (long)109.0) {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs2_target_gb_bs3_bmp_bs3_popup", entity_position.second, &_new_obj));
		}
		else {
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs2_visual_arrow_red", entity_position.second, &_new_obj));
		}

		SDKCheck(Gears::API::EnvironmentAPIv1()->AlignToTerrain(_new_obj));

		if (tank_object)
		{
			CreateTankCrewAndTurret(_new_obj, entity_position.second, csaObjID);
		}

		//Heading
		SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(_new_obj, &orientation));
		orientation.yaw = heading * radsToDegs;
		SDKCheck(Gears::API::TransformationAspectAPIv4()->SetOrientation(_new_obj, orientation));

		AddNewObject(_new_obj, (csaObjID));

		// Disable collision for all objects
		SDKCheck(Gears::API::ModelAspectAPIv1()->EnableGeometry(_new_obj, kGeometryType_collision, disable));

	}
	return entity_position.first;
}

void GenerationView::CreateTankCrewAndTurret(ObjectHandle_v3 vehicle, GeoPosition_v5 position, long csaObjID)
{
	SDKCheck(Gears::API::WorldAPIv5()->CreateObject("VBS2_GB_Army_AFVCrew_M_L22A2_None_CrewHelmet_None", position, &_driver));

	_vehicle_driver.push_back(_driver);
	_vehicle_with_driver.push_back(vehicle);

	// Get vehicle crew positions
	int num_positions = 0;

	SDKCheck(Gears::API::CrewAspectAPIv2()->GetCrewPositions(vehicle, nullptr, &num_positions));

	std::vector<CrewPosition_v1> positions(num_positions);

	SDKCheck(Gears::API::CrewAspectAPIv2()->GetCrewPositions(vehicle, positions.data(), &num_positions));

	// Assign driver
	for (int i = 0; i < num_positions; i++)
	{
		if (positions[i].type == kCrewPositionType_driver)
		{
			SDKCheck(Gears::API::CrewAspectAPIv2()->SetToPosition(vehicle, i, _driver));
		}
	}

	// Create group for moving tank object
	CreateTankGroup(_no_of_tanks);
	_no_of_tanks += 1;

}

void GenerationView::CreateTankGroup(int32_t no_of_tanks)
{
	ObjectHandle_v3 group = { 0,0 };

	if (no_of_tanks == 0)
	{
		// ownship
		SDKCheck(Gears::API::WorldAPIv5()->CreateGroup(kFaction_blufor, &group));
	}
	else
	{
		SDKCheck(Gears::API::WorldAPIv5()->CreateGroup(kFaction_opfor, &group));
	}

	_tank_group.push_back(group);
	SDKCheck(Gears::API::ORBATAPIv2()->SetSuperiorGroup(_vehicle_driver[no_of_tanks], _tank_group[no_of_tanks]));
}


void GenerationView::AddNewObject(ObjectHandle_v3 obj, long csaObjectId)
{
	auto found = std::find(_vbs_obj_identifier.begin(), _vbs_obj_identifier.end(), obj);
	if (found == _vbs_obj_identifier.end())
	{
		// add to vbs objects
		_vbs_obj_identifier.push_back(obj);
		_csa_obj_identifier.push_back(csaObjectId);

		ShowAndLog("Object added");
	}
	else
	{
		ShowAndLog("Object added already exists");
	}

}

void GenerationView::DeleteObject(long csaObjId)
{
	int index;

	auto foundcsaId = std::find(_csa_obj_identifier.begin(), _csa_obj_identifier.end(), (signed long)csaObjId);
	if (foundcsaId == _csa_obj_identifier.end())
	{
		ShowAndLog("Remove CSA Object ID not found");
	}
	else
	{
		index = (int(std::distance(_csa_obj_identifier.begin(), foundcsaId)));
		// remove from CSA object IDs vector
		_csa_obj_identifier.erase(foundcsaId);

		// Get related VBS object and remove
		ObjectHandle_v3 VBSobj = _vbs_obj_identifier.at(index);
		SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(VBSobj));

		// remove from VBS objects vector
		_vbs_obj_identifier.erase(_vbs_obj_identifier.begin() + index);

		// remove driver and tank group if tank object
		auto foundtank = std::find(_vehicle_with_driver.begin(), _vehicle_with_driver.end(), VBSobj);
		if (foundtank == _vehicle_with_driver.end())
		{
			ShowAndLog("No vehicle driver for this object");
		}
		else
		{
			index = (int(std::distance(_vehicle_with_driver.begin(), foundtank)));
			ObjectHandle_v3 driverobj = _vehicle_driver.at(index);
			SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(driverobj));
			_vehicle_with_driver.erase(_vehicle_with_driver.begin() + index);
			_vehicle_driver.erase(_vehicle_driver.begin() + index);
			_tank_group.erase(_tank_group.begin() + index);
			_no_of_tanks = (int)_vehicle_driver.size();
		}

		ShowAndLog("Object deleted");
	}
}
void GenerationView::DeleteAllObjects()
{
	ScopeLock lock(MessageLock);

	// Clear out all objects
	if (removeAllObjects)
	{
		// Create invisible object for player. VBS always needs a player.
		if (ValidateHandle(_ownship))
		{
			GeoPosition_v5 invisible_position = GetPosition(_ownship);
			SDKCheck(Gears::API::WorldAPIv5()->CreateObject("vbs2_invisible_man_west", invisible_position, &_dead_player));
			SDKCheck(Gears::API::MissionAPIv3()->SetPlayer(_dead_player));
		}
		// Delete VBS models
		for (int i = 0; i < _vbs_obj_identifier.size(); i++)
		{
			SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(_vbs_obj_identifier.at(i)));
		}

		for (int i = 0; i < _vehicle_driver.size(); i++)
		{
			SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(_vehicle_driver.at(i)));
		}

		if (ValidateHandle(_driver))
		{
			SDKCheck(Gears::API::WorldAPIv5()->DeleteObject(_driver));
			_driver = kNullObjectHandle;
		}

		_csa_obj_identifier.clear();
		_vbs_obj_identifier.clear();
		entityGoToObjectIdentifier.clear();
		entityGoToLocation.clear();
		entityGoToSpeed.clear();
		entityGoToDwellTime.clear();
		_vehicle_driver.clear();
		_vehicle_with_driver.clear();
		_tank_group.clear();
		_no_of_tanks = 0;
		removeAllObjects = false;
		ShowAndLog("ALL objects deleted");
	}
}

void GenerationView::ControlObjectPosition(long csaObjId, int entityGoToID)
{
	GeoPosition_v5 goto_position = { 0,0,0 };
	Vector3f32_v3 velocity = { 0, 0, 0 };
	int index = 0;

	auto foundObjId = std::find(_csa_obj_identifier.begin(), _csa_obj_identifier.end(), (signed long)csaObjId);
	if (foundObjId == _csa_obj_identifier.end())
	{
		ShowAndLog("Exgen visual object to move not found");
	}
	else
	{
		// Get object number to access correct tank group
		index = (int(std::distance(_csa_obj_identifier.begin(), foundObjId)));

		ConvertENUToLLA(entityGoToLocation[entityGoToID].ECEF_X, entityGoToLocation[entityGoToID].ECEF_Y, entityGoToLocation[entityGoToID].ECEF_Z);
		goto_position = (NewPosition(latLongPosition.latitude, latLongPosition.longitude)).second;

		ObjectHandle_v3 waypoint;
		SDKCheck(Gears::API::WorldAPIv5()->CreateWaypoint(&waypoint));
		SDKCheck(Gears::API::WaypointAspectAPIv2()->SetType(waypoint, kWaypointType_move));
		SDKCheck(Gears::API::WaypointAspectAPIv2()->SetAssignedTo(waypoint, _tank_group[index], -1));

		SDKCheck(Gears::API::TransformationAspectAPIv4()->SetPosition(waypoint, goto_position));

		// Received entity speed in Kilometres/hour
		// model velocity:	Object velocity in model space (right, up, forward) in meters per second
		SDKCheck(Gears::API::TransformationAspectAPIv4()->GetModelVelocity(_vbs_obj_identifier[index], &velocity));
		float32_t speed_mps = (float32_t)(entityGoToSpeed[entityGoToID] * KMHToMPS);
		velocity.z = speed_mps;
		SDKCheck(Gears::API::TransformationAspectAPIv4()->SetModelVelocity(_vbs_obj_identifier[index], velocity));


		ShowAndLog("Exgen object moved");
	}
}

bool GenerationView::UpdateObject(long csaObjId, float64_t ipLat, float64_t ipLon, double heading)
{
	RotationalAngles_v3 orientation;
	int index;

	std::pair<bool, GeoPosition_v5> position = NewPosition(ipLat, ipLon);

	if (position.first)
	{
		auto foundObjId = std::find(_csa_obj_identifier.begin(), _csa_obj_identifier.end(), (signed long)csaObjId);
		if (foundObjId == _csa_obj_identifier.end())
		{
			ShowAndLog("Update visual object not found");
		}
		else
		{
			index = (int(std::distance(_csa_obj_identifier.begin(), foundObjId)));

			// Get related VBS object and move
			ObjectHandle_v3 obj = _vbs_obj_identifier.at(index);

			const GeoPosition_v5 new_position = { position.second.latitude,
												  position.second.longitude,
												  position.second.altitude };

			SDKCheck(Gears::API::TransformationAspectAPIv4()->SetPosition(obj, new_position));
			SDKCheck(Gears::API::EnvironmentAPIv1()->AlignToTerrain(obj));
			//Heading
			SDKCheck(Gears::API::TransformationAspectAPIv4()->GetOrientation(obj, &orientation));
			orientation.yaw = heading * radsToDegs;
			SDKCheck(Gears::API::TransformationAspectAPIv4()->SetOrientation(obj, orientation));

			ShowAndLog("Update object position");
		}
	}

	return position.first;
}

std::pair<bool, GeoPosition_v5> GenerationView::NewPosition(const float64_t& ipLat, const float64_t& ipLon)
{
	bool success = false;
	GeoPosition_v5 position;
	float64_t Alt = 0.0;
	float64_t* ipAlt = &Alt;

	position.latitude = ipLat;
	position.longitude = ipLon;

	APIResult result = Gears::API::EnvironmentAPIv1()->GetTerrainElevation(ipLat, ipLon, ipAlt);
	if (result == kAPIResult_GeneralSuccess)
	{
		success = true;
		position.altitude = Alt;
	}

	ShowAndLog("New position");

	return std::make_pair(success, position);
}

bool GenerationView::ObjectAlreadyExists(long csaObjectId)
{
	if (_csa_obj_identifier.size() > 0)
	{
		auto foundObjID = std::find(_csa_obj_identifier.begin(), _csa_obj_identifier.end(), (signed long)csaObjectId);
		if (foundObjID == _csa_obj_identifier.end())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
}

bool GenerationView::RepositionCamera(StealthUpdate::S_ECEF_POSITION position)
{
	ConvertENUToLLA(position.ECEF_X, position.ECEF_Y, position.ECEF_Z);

	// Get altitude for the position
	std::pair<bool, GeoPosition_v5> camera_pos = NewPosition(latLongPosition.latitude, latLongPosition.longitude);

	if (camera_pos.first)
	{
		if (cameraHeightoffset < MIN_HEIGHT_OFFSET)
			cameraHeightoffset = MIN_HEIGHT_OFFSET;
		camera_pos.second.altitude += cameraHeightoffset;

		_camera->DDSRepositionCamera(camera_pos.second);
	}
	return camera_pos.first;
}

void GenerationView::AlignCamera(StealthUpdate::S_ECEF_POSITION position)
{
	double headingDeltaX = position.ECEF_X - stealthObjectLocation.ECEF_X;
	double headingDeltaY = position.ECEF_Y - stealthObjectLocation.ECEF_Y;
	double alignmentHeading = radsToDegs * atanf(float(headingDeltaX / headingDeltaY));
	double rangeFromStealth = fabs(headingDeltaY / sin(alignmentHeading));

	if (headingDeltaY < 0.0)
	{
		if (headingDeltaX < 0.0)
			alignmentHeading = 180.0 + alignmentHeading;
		else
			alignmentHeading = 180 - (-1.0 * alignmentHeading);
	}

	double alignmentPitch = radsToDegs * atanf(float((position.ECEF_Z - stealthObjectLocation.ECEF_Z) / rangeFromStealth));
	_camera->DDSReorientateCamera(alignmentHeading, alignmentPitch);
}

void GenerationView::UpdateCamera(const float& timeDelta)
{
	if (_camera)
	{
		CalculateCameraMovement(timeDelta);
		if (totalDistanceTravelled != 0.0)
		{
			MoveCameraForward(totalDistanceTravelled);
			totalDistanceTravelled = 0.0;
		}

		CalculateCameraOrientation(timeDelta);
		if (newCameraOrientation)
		{
			_camera->DDSReorientateCamera(radsToDegs * stealthHeading, radsToDegs * currentCameraPitch);
			newCameraOrientation = false;
			cameraUpdated = true;
		}

		if (cameraUpdated)
		{
			sendUpdatesToCIOS();
			cameraUpdated = false;
		}
	}
}

void GenerationView::UpdateCameraView(float delta)
{
	ScopeLock lock(MessageLock);
	if (_camera && setViewToEntityID)
	{
		ObjectHandle_v3 lockedTo;
		Vector3f32_v3 offset;
		int index = 0;

		SDKCheck(Gears::API::CameraAspectAPIv2()->GetLockedObject(_camera->GetHandle(), &lockedTo, &offset));

		auto log_api = Gears::API::LogAPIv1();

		auto foundObjId = std::find(_csa_obj_identifier.begin(), _csa_obj_identifier.end(), (signed long)setViewToEntityID);
		if (foundObjId == _csa_obj_identifier.end())
		{
			ShowAndLog("DDS set view visual object not found");
		}
		else
		{
			index = (int(std::distance(_csa_obj_identifier.begin(), foundObjId)));
			// Get related VBS object 
			_new_obj = _vbs_obj_identifier.at(index);

			ShowAndLog("DDS set view to object");
		}

		if (lockedTo == kNullObjectHandle) // If not locked -> lock
		{
			offset.x = 0; offset.y = 2; offset.z = 0;

			SDKCheck(Gears::API::CameraAspectAPIv2()->LockToObject(_camera->GetHandle(), _new_obj, &offset));

			log_api->LogInfo(log_api, "Camera locked");
		}
		else // Unlock
		{
			SDKCheck(Gears::API::CameraAspectAPIv2()->LockToObject(_camera->GetHandle(), kNullObjectHandle, nullptr));

			log_api->LogInfo(log_api, "Camera unlocked");
		}

		setViewToEntityID = 0;
	}
}

void GenerationView::CalculateCameraMovement(const float& timeDelta)
{
	if (currentStealthSpeed < requiredStealthSpeed)
	{
		double timeToSpeedUp = (requiredStealthSpeed - currentStealthSpeed) / MAX_ACCELERATION;

		if (timeToSpeedUp < timeDelta)
		{
			double distanceTravelledWhileAccelerating = (currentStealthSpeed * timeToSpeedUp) + (0.5 * MAX_ACCELERATION) * (timeToSpeedUp * timeToSpeedUp);
			totalDistanceTravelled = distanceTravelledWhileAccelerating + requiredStealthSpeed * (timeDelta - timeToSpeedUp);
			currentStealthSpeed = requiredStealthSpeed;
		}
		else
		{
			totalDistanceTravelled = (currentStealthSpeed * timeDelta) + (0.5 * MAX_ACCELERATION) * (timeDelta * timeDelta);
			currentStealthSpeed = currentStealthSpeed + (MAX_ACCELERATION * timeDelta);
		}
	}
	else if (currentStealthSpeed > requiredStealthSpeed)
	{
		double timeToSlowDown = (requiredStealthSpeed - currentStealthSpeed) / (-1.0 * MAX_ACCELERATION);

		if (timeToSlowDown < timeDelta)
		{
			double distanceTravelledWhileDeceleratiing = (currentStealthSpeed * timeToSlowDown) + (-0.5 * MAX_ACCELERATION) * (timeToSlowDown * timeToSlowDown);
			totalDistanceTravelled = distanceTravelledWhileDeceleratiing + requiredStealthSpeed * (timeDelta - timeToSlowDown);
			currentStealthSpeed = requiredStealthSpeed;
		}
		else
		{
			totalDistanceTravelled = (currentStealthSpeed * timeDelta) + (-0.5 * MAX_ACCELERATION) * (timeDelta * timeDelta);
			currentStealthSpeed = currentStealthSpeed + (-1.0 * MAX_ACCELERATION * timeDelta);
		}
	}
	else
	{
		totalDistanceTravelled = currentStealthSpeed * timeDelta;
	}
}

void GenerationView::MoveCameraForward(const double& distance)
{
	ScopeLock lock(MessageLock);

	const float64_t stepX = sin(stealthHeading) * distance;
	const float64_t stepY = cos(stealthHeading) * distance;

	stealthObjectLocation.ECEF_X += stepX;
	stealthObjectLocation.ECEF_Y += stepY;
	cameraUpdated = RepositionCamera(stealthObjectLocation);
}

void GenerationView::CalculateCameraOrientation(const float& timeDelta)
{
	ScopeLock lock(MessageLock);
	if (currentHeadingRate != 0.0)
	{
		stealthHeading = stealthHeading + currentHeadingRate * timeDelta;
		newCameraOrientation = true;
	}

	if (currentPitchRate != 0.0)
	{
		currentCameraPitch = currentCameraPitch + currentPitchRate * timeDelta;
		newCameraOrientation = true;
	}
}

void GenerationView::ConvertLLACameraPositionToENU(void)
{
	GeoPosition_v5 latLongCameraPosition = _camera->DDSGetCameraPosition();
	Vector3f64_v3 mapPosition{ 0.0, 0.0, 0.0 };
	SDKCheck(Gears::API::WorldAPIv5()->GeoPositionToMapPosition(latLongCameraPosition, &mapPosition));

	stealthObjectLocation.ECEF_X = mapPosition.x - mapOrigin.x;
	stealthObjectLocation.ECEF_Y = mapPosition.z - mapOrigin.z;
	stealthObjectLocation.ECEF_Z = cameraHeightoffset;

	ShowAndLog("DDS LLA To ENUF");
}

void GenerationView::ConvertENUToLLA(const double& x, const double& y, const double& z)
{
	Vector3f64_v3 mapPosition{ x + mapOrigin.x, z, y + mapOrigin.z };
	SDKCheck(Gears::API::WorldAPIv5()->MapPositionToGeoPosition(mapPosition, &latLongPosition));
	////ShowAndLog("DDS ENU To ECEF");
}

void GenerationView::ProcessStealthControlInputs(void)
{
	ScopeLock lock(MessageLock);

	if (_camera)
	{
		if (stealthRepositionCamera)
		{
			cameraUpdated = RepositionCamera(stealthObjectLocation);
			if (cameraUpdated)
				stealthRepositionCamera = false;
		}
		
		if (stealthReorientateCamera)
		{
			_camera->DDSReorientateCamera(radsToDegs * stealthHeading, radsToDegs * currentCameraPitch);
			stealthReorientateCamera = false;
		}

		if (stealthHeightChange)
		{
			auto iterator = stealthHeightOffset.begin();
			while (iterator != stealthHeightOffset.end())
			{
				cameraHeightoffset = ((*iterator) * (MAX_HEIGHT_OFFSET - MIN_HEIGHT_OFFSET)) + MIN_HEIGHT_OFFSET;
				cameraUpdated = RepositionCamera(stealthObjectLocation);
				if (cameraUpdated)
					iterator = stealthHeightOffset.erase(iterator);
				else
					iterator++;
			}
			if (stealthHeightOffset.empty())
				stealthHeightChange = false;
		}

		if (stealthSpeedChange)
		{
			requiredStealthSpeed = stealthSpeedInput * MAX_SPEED;
			stealthSpeedChange = false;
		}

		if (stealthZoom)
		{
			double zoomLevel = 0.0;

			for (int i = 0; i < stealthZoomLevel.size(); i++)
			{
				zoomLevel = 1 + (stealthZoomLevel[i] * (MAX_ZOOM_LEVEL - 1));
				_camera->DDSUpdateFov(zoomLevel);
			}
			stealthZoomLevel.clear();
			stealthZoom = false;
			cameraUpdated = true;
		}

		if (stealthRotation)
		{
			for (int i = 0; i < stealthHeadingRate.size(); i++)
			{
				currentHeadingRate = stealthHeadingRate[i] * MAX_HEADING_RATE;
			}

			for (int i = 0; i < stealthPitchRate.size(); i++)
			{
				currentPitchRate = stealthPitchRate[i] * MAX_PITCH_RATE;
			}
			stealthPitchRate.clear();
			stealthHeadingRate.clear();
			stealthRotation = false;
		}

		if (stealthAlignCamera)
		{
			AlignCamera(stealthAlignmentLocation);
			cameraUpdated = true;
			stealthAlignCamera = false;
		}
	}
}

void GenerationView::sendUpdatesToCIOS()
{
	ConvertLLACameraPositionToENU();

	RotationalAngles_v3 stealth_camera_orientation = _camera->DDSGetCameraOrientation();
	double cameraHeading = stealth_camera_orientation.yaw < 0 ? stealth_camera_orientation.yaw + 360 : stealth_camera_orientation.yaw;
	double bodyHeading = degsToRads * cameraHeading;
	double pitch = stealth_camera_orientation.pitch;

	stealthUpdateTransmitterCopy->StealthBodyUpdate(stealthObjectLocation, bodyHeading, pitch);
	stealthUpdateTransmitterCopy->StealthCameraHeadingUpdate(bodyHeading);
}

void GenerationView::SelectDeselectObjects(long objID, bool select)
{
	// Mark/Unmark selected object in visual scene
	int index;

	auto foundObjId = std::find(_csa_obj_identifier.begin(), _csa_obj_identifier.end(), (signed long)objID);
	if (foundObjId == _csa_obj_identifier.end())
	{
		SetSelected(kNullObjectHandle);
		ShowAndLog("Select visual object not found");
	}
	else
	{
		index = (int(std::distance(_csa_obj_identifier.begin(), foundObjId)));

		// Get related VBS object and select/deselect
		ObjectHandle_v3 obj = _vbs_obj_identifier.at(index);

		if (select) {
			SetSelected(obj);
			ShowAndLog("Object selected");
		}
		else {
			SetSelected(kNullObjectHandle);
			//ShowAndLog("Object deselected");
		}

	}
}

void GenerationView::UpdateMarker()
{
	if (_selected != kNullObjectHandle)
	{
		const auto pos = GetPosition(_selected);
		Vector3f32_v3 scale = { 1.0F,   // x
								1.0F,   // y
								1.0F }; // z

		SDKCheck(Gears::API::TransformationAspectAPIv4()->GetScale(_selected, &scale));

		const GeoPosition_v3 start = { pos.latitude,                     // latitude
									   pos.longitude,                    // longitude
									   pos.altitude + (4.0 * scale.y) }; // altitude

		if (_selectedMarker < 0)
		{
			const GeoPosition_v3 end = { pos.latitude,                     // latitude
										 pos.longitude,                    // longitude
										 pos.altitude + (6.0 * scale.y) }; // altitude
			const Color_v3 color = { 1.0F,   // r
									 0.0F,   // g
									 0.0F,   // b
									 0.8F }; // a

			SDKCheck(Gears::API::WorldDrawAPIv3()->CreateArrow(start, end, 2.0F, 2.0F, 2.0F, color, &_selectedMarker));
		}
		else
		{
			SDKCheck(Gears::API::WorldDrawAPIv3()->SetPosition(_selectedMarker, start));
		}
	}
	else if (_selectedMarker >= 0)
	{
		SDKCheck(Gears::API::WorldDrawAPIv3()->DeletePrimitive(_selectedMarker));

		_selectedMarker = -1;
	}
}

void GenerationView::Cleanup()
{
  // clear message flags
  exgenLeave = false;
  exgenStart = false;
  stealthZoomLevel.clear();
  stealthBodyHeadingRate.clear();
  stealthBodyPitchRate.clear();
  stealthHeadingRate.clear();
  stealthPitchRate.clear();
  stealthHeightOffset.clear();
  stealthHeading = 0.0;
  stealthObjectLocation = { 0.0, 0.0, 0.0 };
  stealthAlignmentLocation = { 0.0, 0.0, 0.0 };
  stealthRepositionCamera = false;

  _mission_started = false;
  _in_mission = false;
  SetSelected(kNullObjectHandle);
  
  if (_camera != nullptr)
  {
    delete _camera;
  }
  _camera = nullptr;
  _lastXY.x = 0.0F;
  _lastXY.y = 0.0F;

  SDKCheck(Gears::API::MissionAPIv3()->EndMission());

  auto log_api = Gears::API::LogAPIv1();
  log_api->LogInfo(log_api, "Unloaded EXGEN.");
}

bool GenerationView::OnSize(const int32_t client_width, const int32_t client_height)
{
  _client_width = client_width;
  _client_height = client_height;
  return false;
}

bool GenerationView::OnKeyPressed(KeyID key_id, BOOL system_key, uint16_t scan_code, uint16_t scan_flags)
{
	// Check that there is some selected object 
	if (_csa_obj_identifier.size() == 0)
	{
		return false;
	}

	if (key_id == 'G')
	{
		EntityControl::S_ECEF_POSITION loc = {25070.0, 09165.0, 0.0};
		entityGoToObjectIdentifier.push_back(_csa_obj_identifier[1]);
		entityGoToLocation.push_back(loc);
		entityGoToSpeed.push_back(0.0);
		entityGoToDwellTime.push_back(0.0);

		ShowAndLog("Go To setup");

		}
		return false;
}

bool GenerationView::OnMouseButtonPressed(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
  return false;
}

bool GenerationView::OnMouseDoubleClick(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
  if (!_in_mission)
  {
    return false;
  }


  return false;
}

bool GenerationView::OnMouseButtonReleased(ButtonID_v1 button_id, int32_t client_x, int32_t client_y)
{
  _buttons &= ~button_id;
  return false;
}

void GenerationView::Move(const Vector3f64_v3& param)
{
  const auto diff = _camera->TransformXZ(param);
  const auto current_position = GetPosition(_selected);
  const GeoPosition_v5 new_position = { current_position.latitude + diff.z,   // latitude
                                        current_position.longitude + diff.x,  // longitude
                                        current_position.altitude + diff.y }; // altitude

  SDKCheck(Gears::API::TransformationAspectAPIv4()->SetPosition(_selected, new_position));
}

void GenerationView::Rotate(const RotationalAngles_v3& diff)
{
  const auto current_orientation = GetOrientation(_selected);
  const RotationalAngles_v3 new_orientation = { current_orientation.yaw + diff.yaw,     // yaw
                                                current_orientation.pitch + diff.pitch, // pitch
                                                current_orientation.roll + diff.roll }; // roll

  SDKCheck(Gears::API::TransformationAspectAPIv4()->SetOrientation(_selected, new_orientation));
}

void GenerationView::SetSelected(ObjectHandle_v3 object)
{
  if (_selected == object)
  {
    return;
  }

  if (_selected != kNullObjectHandle)
  {
    const Vector3f32_v3 vzero = { 0.0F, 0.0F, 0.0F };
  }

  _selected = object;
  UpdateMarker();

  //const auto msg = object != kNullObjectHandle ? "Object selected." : "Object deselected.";

  //ShowAndLog(msg);
}

void GenerationView::AttachTo(ObjectHandle_v3 parent)
{
  if (_selected != kNullObjectHandle)
  {
    SDKCheck(Gears::API::TransformationAspectAPIv4()->SetAttachedTo(_selected, parent));

    //const auto msg = parent != kNullObjectHandle ? "Object attached." : "Object detached.";

    //ShowAndLog(msg);
  }
}

void GenerationView::SQFDisableActionMenu()
{
	const char* bind_nextAction = "nextActionKeyBind = [\"NextAction\",false,0] bindKeyEx \"true\"";
	const char* bind_prevAction = "prevActionKeyBind = [\"PrevAction\",false,0] bindKeyEx \"true\"";
	const char* bind_defAction = "DeftActionKeyBind = [\"Action\",false,0] bindKeyEx \"true\";";

	char command_result[512];
	int32_t command_result_length = 512;
	int32_t error_position = -1;
	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(bind_nextAction, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing the script command:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}

	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(bind_prevAction, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing the script command:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}

	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(bind_defAction, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing the script command:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}
}

void GenerationView::SQFEnabledActionMenu()
{
	const char* unbind = "{unBindKey _x} forEach [nextActionKeyBind,prevActionKeyBind,DeftActionKeyBind  ];";

	char command_result[512];
	int32_t command_result_length = 512;
	int32_t error_position = -1;
	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(unbind, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing the script command:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}
}

void GenerationView::SQFDisableUserInput()
{
	const char* disableUserInput = "disableUserInput true;";

	char command_result[512];
	int32_t command_result_length = 512;
	int32_t error_position = -1;
	SDKCheck(Gears::API::ScriptAPIv3()->ExecuteSQF(disableUserInput, command_result, &command_result_length, &error_position));
	if (error_position >= 0)
	{
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), "Something went wrong when executing SQFDisableUserInput:");
		Gears::API::LogAPIv1()->LogWarning(Gears::API::LogAPIv1(), command_result);
	}
}
