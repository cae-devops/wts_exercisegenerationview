#include "DDSExerciseGeneration.h"
#include "GenerationMessageData.h"
#include "GenerationView.h"
#include "ScopeLock.h"

void DDSExerciseGeneration::DisplayVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier, const long& objectSubTypeId, const S_OBJECT_LOCATION_AND_HEADING& locationAndheading)
{
	ScopeLock lock(MessageLock);
	// " *** DDSExerciseGeneration::DisplayVisualObject ***" 
	// "     Object Id :          = 8 " 
	// "     Object Type Id :     = 4 " 
	// "     Object Sub Type Id : = 101 " 
	// "     Position X :         = 5.556 " 
	// "     Position Y :         = 10001.123 " 
	// "     Position Z :         = 1.9998 "
	// "     Heading :            = 359.9 " 

	exgenAddObjectIDs.push_back(objectIdentifier.objectId);
	exgenAddObjectIdentifier.push_back(objectIdentifier);
	exgenObjectSubTypeId.push_back(objectSubTypeId);
	exgenAddObjectLocationAndHeading.push_back(locationAndheading);
	addObjects = true;
}

void DDSExerciseGeneration::DeselectVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier)
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::DeselectVisualObject ***" << std::endl;
	//std::cout << "     Object Id : " << objectIdentifier.objectId << std::endl;
	//std::cout << "     Object Type Id : " << objectIdentifier.objectTypeId << std::endl;

	exgenDeselectObjectIdentifier = objectIdentifier;
}
void DDSExerciseGeneration::LoadTerrainDatabase(const unsigned long& terrainId, const std::string& terrainName)
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::LoadTerrainDatabase ***" << std::endl;
	//std::cout << "     Terrain Id : " << terrainId << std::endl;
	//std::cout << "     Terrain Name : " << terrainName << std::endl;
}

void DDSExerciseGeneration::RemoveVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier)
{
	ScopeLock lock(MessageLock);
    // ********************** NOT USED *********************
	// " *** DDSExerciseGeneration::RemoveVisualObjects ***" 
	// "     Object Id :          = 8 " 
	// "     Object Type Id :     = 4 " 
	//exgenRemoveObjectIdentifier = objectIdentifier;
}

void DDSExerciseGeneration::RemoveVisualObjects(const unsigned long& numOfObjects, const S_UNIQUE_OBJECT_IDENTIFIER *objectIdentifiers)
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::RemoveVisualObjects ***" << std::endl;

	// Mass clearout. (Equals 0 for now, message problem TBD)
	if (numOfObjects > 999 || numOfObjects == 0)
	{
		removeAllObjects = true;
	}
	else  
	{   
		for (unsigned long i = 0; i < numOfObjects; i++)
	    {
			exgenRemoveObjectIDs.push_back(objectIdentifiers[i].objectId);
		}
	}
}

void DDSExerciseGeneration::UpdateVisualObjectPosition(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier, const S_OBJECT_LOCATION_AND_HEADING& locationAndheading)
{
	ScopeLock lock(MessageLock);
	// " *** DDSExerciseGeneration::UpdateVisualObjectPosition ***" 
	// "     Object Id :          = 8 " 
	// "     Object Type Id :     = 4 " 
	// "     Position X :         = 5.556 " 
	// "     Position Y :         = 10001.123 " 
	// "     Position Z :         = 1.9998 "
	// "     Heading :            = 359.9 " 

	exgenUpdateObjectIdentifier = objectIdentifier;
	exgenUpdateObjectLocationAndHeading = locationAndheading;

}

void DDSExerciseGeneration::SelectVisualObject(const S_UNIQUE_OBJECT_IDENTIFIER& objectIdentifier)
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::SelectVisualObject ***" << std::endl;
	//std::cout << "     Object Id : " << objectIdentifier.objectId << std::endl;
	//std::cout << "     Object Type Id : " << objectIdentifier.objectTypeId << std::endl;

	exgenSelectObjectIdentifier = objectIdentifier;
}

void DDSExerciseGeneration::LoadTrack(const std::string& TrackName, const std::string& trackFile)
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::LoadTrack ***" << std::endl;
	//std::cout << "     Track Name : " << TrackName << std::endl;
	//std::cout << "     Track File : " << trackFile << std::endl;
	exgenTrackName = TrackName;
	exgenTrackFile = trackFile;
}
void DDSExerciseGeneration::PauseTrack()
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::PauseTrack ***" << std::endl;
	exgenPauseTrack = true;
}

void DDSExerciseGeneration::ResumeTrack()
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::ResumeTrack ***" << std::endl;
	exgenResumeTrack = true;
}

void DDSExerciseGeneration::StartTrack()
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::StartTrack ***" << std::endl;
	exgenStartTrack = true;
}

void DDSExerciseGeneration::StopTrack()
{
	ScopeLock lock(MessageLock);
	//std::cout << " *** DDSExerciseGeneration::StopTrack ***" << std::endl;
	exgenStopTrack = true;
}
