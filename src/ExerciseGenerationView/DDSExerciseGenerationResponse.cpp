#include "DDSExerciseGenerationResponse.h"

void DDSExerciseGenerationResponse::LoadTerrainDatabaseResponse(const bool& success)
{
	//std::cout << " *** DDSExerciseGenerationResponse::LoadTerrainDatabaseResponse ***" << std::endl;
	//std::cout << "     Success " << success << std::endl;
}

void DDSExerciseGenerationResponse::LoadTrackResponse(const bool& success)
{
	//std::cout << " *** DDSExerciseGenerationResponse::LoadTrackResponse ***" << std::endl;
	//std::cout << "     Success " << success << std::endl;
}

void DDSExerciseGenerationResponse::PauseTrackResponse(const bool& success)
{
	//std::cout << " *** DDSExerciseGenerationResponse::PauseTrackResponse ***  " << std::endl;
	//std::cout << "     Success " << success << std::endl;
}

void DDSExerciseGenerationResponse::RemoveVisualObjectsResponse(const bool& success)
{
	//std::cout << " *** DDSExerciseGenerationResponse::RemoveVisualObjectsResponse ***" << std::endl;
	//std::cout << "     Success " << success << std::endl;
}

void DDSExerciseGenerationResponse::ResumeTrackResponse(const bool& success)
{
	//std::cout << " *** DDSExerciseGenerationResponse::ResumeTrackResponse ***" << std::endl;
	//std::cout << "     Success " << success << std::endl;
}

void DDSExerciseGenerationResponse::StartTrackResponse(const bool& success)
{
	//std::cout << " *** DDSExerciseGenerationResponse::StartTrackResponse ***" << std::endl;
	//std::cout << "     Success " << success << std::endl;
}

void DDSExerciseGenerationResponse::StopTrackResponse(const bool& success)
{
	//std::cout << " *** DDSExerciseGenerationResponse::StopTrackResponse ***" << std::endl;
	//std::cout << "     Success " << success << std::endl;
}
